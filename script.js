const link = document.querySelector("#link");
const send = document.querySelector(".btnShort");
const shortenContainer = document.querySelector(".shortenContainer");
let dataLocal = JSON.parse(localStorage.getItem("dataArr"));
console.log(dataLocal);

const affiche = function (originalUrl, shortenUrl) {
  shortenContainer.innerHTML += `<div class="linkShorted">
  <p class="originalLInk">${originalUrl}</p>
  <div class="linkShortedRight">
    <p class="newLInk">${shortenUrl}</p>
    <button class="btnCopy">copy</button>
  </div>
</div>`;
};

if (dataLocal !== null) {
  affiche(dataLocal[0], dataLocal[1]);
}

const shortUrl = async (e) => {
  e.preventDefault();
  const url = link.value;
  const response = await fetch(`https://api.shrtco.de/v2/shorten?url=${url}`);
  const data = await response.json();
  affiche(data.result.original_link, data.result.full_short_link);
  let arr = [data.result.original_link, data.result.full_short_link];
  console.log(arr);
  localStorage.setItem("dataArr", JSON.stringify(arr));
};
send.addEventListener("click", shortUrl);

let copy = document.querySelector(".btnCopy");
copy.addEventListener("click", function () {
  var copylink = document.querySelector(".newLInk");
  console.log(copylink.textContent);
  navigator.clipboard.writeText(copylink.textContent);
});
